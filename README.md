# Nextel To-Do App

A simple to-do app to organize your daily tasks.

## Getting Started

Clone the repository, access the folder via terminal and run:

```
$ sudo npm install
```
To install the dependencies.
<br>
<hr>
<br>
After install all dependencies run:

```
$ npm start
```
To serve the project on `http://localhost.com:3000`

Have fun!

## License:
MIT