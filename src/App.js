import React, { Component } from 'react';
import Task from './components/Task/Task';
import styled from 'styled-components';

const Todo = styled.div`
  width:  100%;
`;

class App extends Component {

  render() {
    return (
      <Todo>
        <Task />
      </Todo>
    );
  }
}

export default App;
