import React from 'react';
import styled from 'styled-components';
import TaskForm from './TaskForm';
import TaskList from './TaskList';

const TaskComponent = styled.div`
  max-width:      610px;
  margin:         20px auto;
  padding:        20px;
  border:         1px solid #ddd;
  border-radius:  10px;
`;

class Task extends React.Component {

  state = {
    tasks : [
      {name: 'work', done: false},
      {name: 'study', done: false},
      {name: 'develop', done: false}
    ]
  }

  getTasks (src){
    return Object.assign(src);
  }

  addTask (e) {

    e.preventDefault();

    const newTask = {name: e.target.taskName.value};
    const setNewTask = this.getTasks(this.state.tasks).concat(newTask);

    this.setState({
      tasks : setNewTask
    })

    e.target.taskName.value = null; // Clear input value

  }

  deleteTask (TaskName) {

    const tasks = this.getTasks(this.state.tasks);
    const updatedTasks = tasks.filter( task => task.name !== TaskName );

    this.setState({
      tasks : updatedTasks
    })

  }

  doneTask (TaskName) {

    const tasks = this.getTasks(this.state.tasks);

    const setTaskDone = () => {
      const updatedTasks = tasks.map( item => {
        if(item.name === TaskName) item.done = !item.done;
        return item;
      })
      return updatedTasks;
    }

    this.setState({
      tasks : setTaskDone()
    })

  }


  render () {
    return (
      <TaskComponent>

        <TaskList 
          tasks={this.state.tasks} 
          delete={this.deleteTask.bind(this)} 
          done={this.doneTask.bind(this)} 
        />
        <TaskForm add={this.addTask.bind(this)}  />

      </TaskComponent>
    )
  } 
}

export default Task;