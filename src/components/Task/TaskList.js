import React from 'react';
import styled from 'styled-components';

const Ul = styled.ul`
  li{
    margin: 10px 0;
  }
`

const Span = styled.span`
  padding:          0 6px;
  margin:           0 4px;
  color:            #fff;
  background-color: #000;
  cursor:           pointer;
`

const TaskList = props => {

  return(
    <Ul>
      {props.tasks.map( index => {
        return (
          <li key={index.name} > 

            <span className={index.done ? 'Completed' : null} >{index.name}</span>

            <Span onClick={ props.delete.bind(this, index.name) }> 
              X
            </Span>

            <Span onClick={ props.done.bind(this, index.name) }> 
              OK
            </Span>

          </li>
        );
      })}
    </Ul>
  )  

}

export default TaskList;