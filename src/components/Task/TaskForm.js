import React from 'react';
import styled from 'styled-components';
import './Task.css';

const Form = styled.form`

    input {
        width:              100%;
        max-width:          280px;
        border:             none;
        border-bottom:      1px solid #000;
        padding-bottom:     5px;
        font-size:          17px;
        outline:            none;
    }

    button{
        font-size:          17px;
        background-color:   #fff;
        appearance:         none;
        position:           relative;
        top:                3px;
        margin-left:        20px;
        outline:            none;
        border:             1px solid #000;
    }

`;



const TaskForm = props => {

    return(

        <Form onSubmit={props.add}>
            <input name="taskName" type="text" placeholder="Type the task name here"/>
            <button type="submit">Add!</button>
        </Form>

    )

}

export default TaskForm;